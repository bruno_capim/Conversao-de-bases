# Algoritmo para Conversão de Base 

## Descrição


## Overview do CÓDIGO

Bibliotecas utilizadas no código:

```c
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <TIME.H>
#include<stdio.h>
```

Para rodar exemplo, use o seguinte valor:

```bash
Números decimais ou inteiros.
```


## Autores
Esse algoritmo foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/10042851/avatar.png?width=400) | Bruno Antunes | [@Bruno_Capim](https://gitlab.com/bruno_capim) | [loginobsequio@gmail.com](mailto:loginobsequio@gmail.com)
| ![](https://gitlab.com/uploads/-/system/user/avatar/9168497/avatar.png?width=400)  | Eduardo José Gnoatto | EduardoGnoatto | [eduardognoatto@alunos.utfpr.edu.br](mailto:eduardognoatto@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/5658313/avatar.png?width=400)  | Lucas Fortes Zillmer | zillmxr | [lucasforteszillmer@alunos.utfpr.edu.br](mailto:lucasforteszillmer@alunos.utfpr.edu.br)





